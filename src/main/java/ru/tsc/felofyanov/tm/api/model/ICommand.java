package ru.tsc.felofyanov.tm.api.model;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();
}
