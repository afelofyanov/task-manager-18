package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {
    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

    Collection<AbstractCommand> getCommands();
}
