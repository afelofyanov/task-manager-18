package ru.tsc.felofyanov.tm.command.system;

public final class AboutCommand extends AbstractSystemCommand {
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[About]");
        System.out.println("Name: Alexander Felofyanov");
        System.out.println("E-mail: afelofyanov@t1-consulting.ru");
    }
}
