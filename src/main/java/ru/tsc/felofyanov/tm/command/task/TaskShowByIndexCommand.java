package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getServiceLocator().getTaskService().findOneByIndex(index);
        showTask(task);
    }
}
