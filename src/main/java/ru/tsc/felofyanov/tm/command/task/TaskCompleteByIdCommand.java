package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");

        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getServiceLocator().getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }
}
