package ru.tsc.felofyanov.tm.command.user;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change user password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = getServiceLocator().getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD]");
        final String password = TerminalUtil.nextLine();
        getServiceLocator().getUserService().setPassword(userId, password);
    }
}
