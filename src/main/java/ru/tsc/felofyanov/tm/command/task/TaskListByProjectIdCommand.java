package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.enumerated.Sort;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task list by project.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }
}
