package ru.tsc.felofyanov.tm.command.system;

import ru.tsc.felofyanov.tm.api.service.ICommandService;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }
}
