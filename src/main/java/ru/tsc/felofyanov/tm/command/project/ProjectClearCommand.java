package ru.tsc.felofyanov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getServiceLocator().getProjectService().clear();
    }
}
