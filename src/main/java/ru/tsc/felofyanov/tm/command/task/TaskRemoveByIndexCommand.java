package ru.tsc.felofyanov.tm.command.task;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getServiceLocator().getTaskService().removeByIndex(index);
    }
}
