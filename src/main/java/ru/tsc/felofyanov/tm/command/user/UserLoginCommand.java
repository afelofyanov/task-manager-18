package ru.tsc.felofyanov.tm.command.user;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Login";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        getServiceLocator().getAuthService().login(login, password);
    }
}
