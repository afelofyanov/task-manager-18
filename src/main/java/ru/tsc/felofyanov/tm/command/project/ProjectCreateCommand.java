package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();

        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        getServiceLocator().getProjectService().create(name, description, dateBegin, dateEnd);
    }
}
