package ru.tsc.felofyanov.tm.command.user;

import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update profile of current user";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        final String userId = getServiceLocator().getAuthService().getUserId();

        System.out.println("ENTER NEW FIRST NAME");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME");
        final String lastName = TerminalUtil.nextLine();

        getServiceLocator().getUserService().updateUser(userId, firstName, middleName, lastName);
    }
}
