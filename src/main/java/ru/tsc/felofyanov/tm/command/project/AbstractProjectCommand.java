package ru.tsc.felofyanov.tm.command.project;

import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        final Status status = project.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }
}
