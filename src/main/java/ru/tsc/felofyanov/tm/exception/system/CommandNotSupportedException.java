package ru.tsc.felofyanov.tm.exception.system;

import ru.tsc.felofyanov.tm.exception.AbstractExcception;

public final class CommandNotSupportedException extends AbstractExcception {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(String message) {
        super("Error! Command [" + message + "] not supported...");
    }
}
