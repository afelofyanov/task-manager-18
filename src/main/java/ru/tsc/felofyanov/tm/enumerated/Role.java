package ru.tsc.felofyanov.tm.enumerated;

public enum Role {
    ADMIN("Administration"),
    USUAL("Usual user");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
